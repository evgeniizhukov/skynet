import { IVariant } from './Variant.model'

export const variantsByFactory = (model: IVariant[]): IVariant[] => {
  return model.map((model) => ({
    title: model.title,
    description: model.description,
    color: model.color,
    price_default: model.price_default,
    options: model.options,
    select: model.select,
  }))
}
