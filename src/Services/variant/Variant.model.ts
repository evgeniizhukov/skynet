export interface Ioption {
  title: string
  price: number
}

export interface Ilux {
  title: string
  items: Ioption[]
}

export interface IVariant {
  title: string
  description: string
  color: string
  price_default: number
  options: Ioption[]
  select: Ilux[]
}
