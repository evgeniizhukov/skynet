import { IVariant } from '../Services/variant/Variant.model'

export interface IPrice {
  connection: IVariant[]
  routes: IVariant[]
  connectionPrice: number
  routerPrice: number
}
