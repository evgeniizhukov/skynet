import { defineStore } from 'pinia'
import { IPrice } from './home.type'
import { IVariant } from '../Services/variant/Variant.model'

export const priceStore = defineStore({
  id: 'appVariants',
  state: (): IPrice => ({
    routes: [
      {
        title: '',
        description: '',
        color: '',
        price_default: 0,
        options: [],
        select: [],
      },
    ],
    connection: [
      {
        title: '',
        description: '',
        color: '',
        price_default: 0,
        options: [],
        select: [],
      },
    ],
    connectionPrice: 0,
    routerPrice: 0,
  }),

  getters: {
    getConnectionPrice(): number {
      return this.connectionPrice
    },

    getRouterPrice(): number {
      return this.routerPrice
    },

    getTotalPrice(): number {
      return this.connectionPrice + this.routerPrice
    },

    getConnection(): IVariant[] {
      return this.connection
    },

    getRoutes(): IVariant[] {
      return this.routes
    },
  },

  actions: {
    setConnection(state: IVariant[]) {
      this.connection = state
    },

    setRoutes(state: IVariant[]) {
      this.routes = state
    },

    setConnectionPrice(newCost: number) {
      this.connectionPrice = newCost
    },

    setRouterPrice(newCost: number) {
      this.routerPrice = newCost
    },
  },
})
